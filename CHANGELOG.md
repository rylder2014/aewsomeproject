# CHANGELOG

Todas as alterações notáveis neste projeto serão documentadas neste arquivo.

## [Versão 1.0.0] - Data de Lançamento (opcional)

### Adicionado

- Nova funcionalidade incrível.
- Outra funcionalidade emocionante.

### Corrigido

- Problema importante que foi resolvido.

## [Versão 0.2.0] - 2023-09-14

### Adicionado

- Novo recurso experimental.

### Alterado

- Melhorias de desempenho na renderização.

### Corrigido

- Correção para um problema de renderização em alguns dispositivos.

## [Versão 0.1.1] - 2023-09-10

### Corrigido

- Correção para um bug crítico no login.

## [Versão 0.1.0] - 2023-09-08

### Adicionado

- Primeira versão funcional do projeto.

