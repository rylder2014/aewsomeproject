# Guia de Uso do Conventional Commits

Este repositório utiliza o estilo de commits conhecido como Conventional Commits para padronizar as mensagens de commit e facilitar o gerenciamento das mudanças no projeto. O Conventional Commits segue um padrão específico que inclui prefixos e descrições para cada commit. Abaixo estão os passos para começar a utilizar o Conventional Commits neste projeto.

## O que são Conventional Commits?

Conventional Commits é uma convenção de mensagens de commit que segue um padrão específico para facilitar a categorização e rastreabilidade das mudanças em um projeto. Cada mensagem de commit deve seguir o seguinte formato:

```
<tipo>(<escopo>): <descrição>
```

- **`<tipo>`**: Indica o tipo de mudança (por exemplo, `feat` para uma nova funcionalidade, `fix` para uma correção, `docs` para documentação, etc.).

- **`<escopo>`** (opcional): Indica a área ou componente do projeto afetado pela mudança.

- **`<descrição>`**: Breve descrição da mudança.


## Por que Usar Conventional Commits?

- **Facilita a Rastreabilidade**: Permite identificar facilmente o propósito de cada alteração.
- **Automatiza a Geração de Changelog**: Ferramentas podem usar as mensagens para gerar um changelog automatizado.
- **Padroniza a Comunicação**: Torna a comunicação entre colaboradores mais clara e eficiente.

## Convenções Comuns de Tipos

Aqui estão algumas das tags iniciais mais comuns utilizadas em mensagens de commit:

- **feat**: Para adicionar uma nova funcionalidade ao código.
- **fix**: Para corrigir um bug ou problema existente.
- **chore**: Tarefas de manutenção, refatoração ou outras atividades que não afetam diretamente o comportamento do código.
- **docs**: Alterações ou adições à documentação do projeto.
- **style**: Atualizações que não afetam a lógica do código (por exemplo: formatação, espaços em branco).
- **test**: Adição ou modificação de testes.
- **perf**: Melhorias de desempenho.
- **revert**: Reverte uma alteração anterior.

## Fazendo Commits

Ao fazer commits no projeto, siga o formato de Conventional Commits. Aqui estão alguns exemplos:

- Para adicionar uma nova funcionalidade:

```bash
git commit -m "feat(auth): add user auth"
```

- Para corrigir um erro:

```bash
git commit -m "fix(api): fix incorrect route"
```

- Para atualizar a documentação:

```bash
git commit -m "docs(readme): update conventional commits readme"
```

Todos os commits devem ser feitos na língua inglesa, seguindo as boas práticas de desenvolvimento.

## Plugin Conventional Commit no Android Studio

Para facilitar o uso de Conventional Commits no Android Studio, a equipe sugere a instalação do plugin "Conventional Commit". Este plugin fornece suporte integrado para o estilo de commits Conventional Commits, oferecendo uma experiência mais fluída e intuitiva para os desenvolvedores.

Para instalar o plugin, siga os passos abaixo:

1. Abra o Android Studio.
2. Vá em `File` > `Settings` > `Plugins`.
3. Pesquise por "Conventional Commit" na barra de busca.
4. Clique em "Install" ao lado do plugin correspondente.

Após a instalação, o Android Studio estará configurado para auxiliá-lo na criação de commits seguindo o padrão Conventional Commits.

## Dicas Adicionais

- Mantenha as mensagens de commit claras e concisas, descrevendo o que a mudança faz.

- Utilize o mesmo prefixo para commits relacionados a uma mesma funcionalidade ou correção.

- Mantenha-se atualizado com as convenções definidas para o projeto e comunique-se com a equipe sobre qualquer alteração nas convenções.

- Lembre-se de que o uso consistente de Conventional Commits ajuda na geração automática de notas de lançamento e no rastreamento de alterações.

Este guia fornece uma visão geral básica do uso de Conventional Commits neste projeto. Se você tiver dúvidas ou precisar de ajuda adicional, sinta-se à vontade para entrar em contato com a equipe de desenvolvimento.

**Boas Contribuições!** 🚀
