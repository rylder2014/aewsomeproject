# Guia de Uso do GitFlow

Este repositório utiliza o GitFlow como metodologia de branching para facilitar o gerenciamento e colaboração no desenvolvimento do projeto. Abaixo estão os passos básicos para começar a trabalhar com o GitFlow neste projeto.

## O que é GitFlow?

O GitFlow é um modelo de branching que define uma estrutura clara para o gerenciamento de branches em um repositório git. Ele propõe o uso de dois branches principais: `main` e `develop`, além de branches de feature, release e hotfix para organizar o desenvolvimento.

### Branches Principais

- **`main`**: Esta é a branch principal e contém apenas o código que foi efetivamente lançado em produção.

- **`develop`**: É a branch onde o desenvolvimento ativo ocorre. Novas features são mescladas aqui antes de serem disponibilizadas em produção.

### Branches Auxiliares

- **`feature/id`**: Branches para desenvolvimento de novas funcionalidades.

- **`release/`**: Branches criadas para preparar um novo lançamento de produção.

- **`hotfix/id`**: Branches para correção rápida de problemas em produção.

O `id` deverá ser o identificador da tarefa desenvolvida. Cada branch **deve** conter ao menos uma tarefa vinculada. Isso auxilia a gestão de Merge Requests e rastreamento de tarefas.

## Como Começar

1. Clone o repositório para o seu ambiente local:

```bash
git clone https://github.com/seu-usuario/seu-repositorio.git
```

2. Certifique-se de que está na branch `develop`:

```bash
git checkout develop
```

## Trabalhando em uma Nova Funcionalidade

1. Crie uma nova branch para sua funcionalidade:

```bash
git checkout -b feature/1-feature-name
```

2. Desenvolva e faça commits na sua branch.

3. Quando sua funcionalidade estiver pronta para revisão, faça um pull request para a branch `develop`.

4. Após a revisão, sua funcionalidade será mesclada na branch `develop`.

## Preparando um Novo Lançamento

1. Crie uma nova branch de release:

```bash
git checkout -b release/vX.Y.Z
```

2. Finalize os preparativos para o lançamento (atualize a documentação, versão, etc.).

3. Faça um pull request para a branch `main` para lançar a versão.

4. Após a revisão, a branch de release será mesclada na `main` e `develop`. A `main` será taggeada com a nova versão.

## Corrigindo Problemas em Produção

1. Crie uma nova branch de hotfix:

```bash
git checkout -b hotfix/2-fix-login-crash
```

2. Corrija o problema.

3. Faça um pull request para a branch `main`.

4. Após a revisão, a branch de hotfix será mesclada na `main` e `develop`.

## Dicas Adicionais

- Sempre mantenha o repositório atualizado com a branch remota antes de começar a trabalhar em uma nova funcionalidade ou correção:

```bash
git pull origin develop
```

- Mantenha commits pequenos e frequentes, com mensagens descritivas.

- Use o rebase para manter um histórico de commits limpo antes de criar um pull request:

```bash
git rebase develop
```

- Lembre-se de sempre seguir as convenções de nomenclatura para branches (`feature/`, `release/`, `hotfix/`).

- Mantenha a documentação atualizada conforme novas funcionalidades são implementadas.

Este guia fornece uma visão geral básica do uso do GitFlow neste projeto. Se você tiver dúvidas ou precisar de ajuda adicional, sinta-se à vontade para entrar em contato com a equipe de desenvolvimento.

**Boas Contribuições!** 🚀
