### Descrição

[Descreva as alterações realizadas de forma clara e concisa.]

### Issue Relacionada

Fixes #NúmeroDaIssue

### Tipo de Alteração

- [ ] Nova Funcionalidade
- [ ] Correção de Bug
- [ ] Melhoria de Desempenho
- [ ] Refatoração
- [ ] Outro (especifique)

### Checklist

- [ ] Eu testei as alterações localmente.
- [ ] Eu atualizei a documentação, se necessário.
- [ ] Todos os testes passaram.
- [ ] A formatação do código está correta.
- [ ] A branch de destino está correta (por exemplo, `main` ou `develop`).

### Capturas de Tela (se aplicável)

[Inclua capturas de tela ou imagens relevantes aqui.]

### Informações Adicionais

[Adicione qualquer outra informação relevante aqui.]

**Versão do Projeto:** [Inserir número da versão ou commit]

**Sistema Operacional:** [Inserir sistema operacional]

**Navegador (se aplicável):** [Inserir navegador e versão]
