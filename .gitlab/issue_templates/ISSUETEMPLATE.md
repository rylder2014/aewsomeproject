## Descrição do Problema

[Descreva aqui o problema de forma clara e concisa.]

### Passos para Reproduzir

1. [Passo 1]
2. [Passo 2]
3. [Passo 3]

### Comportamento Esperado

[Descreva o comportamento que você espera.]

### Comportamento Atual

[Descreva o comportamento atual.]

### Ambiente

- Sistema Operacional: [Inserir sistema operacional]
- Navegador (se aplicável): [Inserir navegador e versão]
- Dispositivo (se aplicável): [Inserir dispositivo e versão]
- Versão App: [Inserir versão do aplicativo]


### Capturas de Tela (se aplicável)

[Inclua capturas de tela aqui.]

### Informações Adicionais

[Adicione qualquer outra informação relevante aqui.]
